# WeatherBot
VK bot that allows you to get weather information at the current time.

## Requirements
- `vk_api`
- `geopy`

## Configuration variables
 _used in the config.py_
- ` TOKEN_VK`
- `TOKEN_APIXUWEATHER`

## Set API keys
At first you should to get api keys of VK and APIXU
- [APIXU API](https://www.apixu.com/signup.aspx)
- [VK API](https://vk.com/dev/access_token)

After that open config.py and write keys into variables
```
    TOKEN_VK='yourVKAPIkey'
    TOKEN_APIXUWEATHER='yourAPIXUAPIkey'
```
## Run

### Manual
```
    python main.py
```

## Logs
You can find logs in weatherBot.log`
    
