import vk_api
import time
import math
import json
from random import randint
from vk_api import VkUpload
from vk_api.longpoll import VkLongPoll, VkEventType
from geopy.geocoders import Nominatim
import requests
import re
from logger import logger
from config import TOKEN_VK
from config import TOKEN_APIXUWEATHER

def get_weather(city):
    try:
        # geolocator = Nominatim(timeout=5)
        # location = geolocator.geocode(city)
        if city != None:
            temp = None
            # response = requests.get('https://api.apixu.com/v1/current.json?key=' + TOKEN_APIXUWEATHER + '&q=' + str(city))
            responseTest = requests.get('http://api.openweathermap.org/data/2.5/weather?q='+str(city)+'&appid=480340689b969d01f7f2ba3c07c9ac4d&lang=ru&units=metric')
            print("RESPONSE TYPE", responseTest.status_code)
            print("RESIPNSE TUPE")
            print(responseTest.content.decode())
            # if response.status_code is 400:
            #     temp = None
            #     print("temp is ", temp)
            if responseTest.status_code is 200:
                print("ZAEBIS'")
                result = json.loads(responseTest.content.decode())
                print("\n", result)
                temp = result
                print("temp is ", temp)
                # return None
            print("temp is ", temp)
            # print("TEMPMPMPMPMPMPM", temp)
            # temp.append(response.content.decode())
            # is_dayResp = response.content.decode()
            # isDay = is_dayResp['current']
            # print("\nTEMPMPMPMPMPMPM", response.content.decode()['current'])
            return temp
    except:
        # logger("ERROR: Nominatim is unavailable")
        # geolocator = Nominatim(timeout=5)
        # location = geolocator.geocode(city)
        if city != None:
            response = requests.get('https://api.apixu.com/v1/current.json?key=' + TOKEN_APIXUWEATHER + '&q=' + str(city))

        result = json.loads(response.content.decode())
        weather = result['current']
        return weather


class VKBot:
    """
    VKBot object
    """
    vk = 0
    vk_session = 0
    session = 0
    upload = 0
    long_poll = 0
    event = 0

    def __init__(self, log=None, passwd=None, token=None):
        if token:
            self.vk_session = vk_api.VkApi(token=TOKEN_VK)
        else:
            self.vk_session = vk_api.VkApi(log, passwd)
            try:
                self.vk_session.auth()
            except vk_api.AuthError as error_msg:
                print(error_msg)
                return
        self.vk = self.vk_session.get_api()
        self.session = requests.session()
        self.upload = VkUpload(self.vk_session)
        self.long_poll = VkLongPoll(self.vk_session)

    def __command_handler__(self, commands, handler):
        message_set = self.event.text.split(u' ')
        print("MESSAGE SET->", message_set, "LEN is", len(message_set))
        if len(message_set) is 2:
            print("messageSet is ->", message_set[1], "len == ", len(message_set))
            # logger("QUERY->"+str(message_set[1] + " " + str(message_set[1])))

        # if len(message_set) is 1:
        #     print("ERROR IN LENGTH")
        #     command_error(self.event, self.vk)
        #     pass
        if bool(re.match(r'^п.?г.?д.?', str(message_set[0]), re.I)) is True:
            if len(message_set) is 2:
                city = message_set[1]
                print("city->", city)
                weather = get_weather(city)
                print("weather->")
                print("weather->", weather)
                handler(self.event, self.vk, weather, city)
                pass
            else:

                if bool(re.match(r'^пр.?в.?т', str(message_set[0]), re.I)) is True:
                    print("HELLO")
                else:
                    command_error(self.event, self.vk)
                    pass
        else:
            print("ERROR!", bool(re.match(r'^п.?г.?д.?', str(message_set[0]), re.I)))
            command_error(self.event, self.vk)
            pass

    def __query_manager__(self, queryset):

        for item in queryset:
            print("\nitems->>", item)
            self.__command_handler__(item[0], item[1])

    def run(self, query):
        # logger("------------------------------------------->")
        # logger("WeatherBot is running now")
        print("WeatherBot is running now")
        try:
            for event in self.long_poll.listen():
                if event.type == VkEventType.MESSAGE_NEW and event.to_me:
                    print(query)
                    self.event = event
                    self.__query_manager__(query)
        except:
            # logger("PROBLEMS with LongPool...Restarting after 10 seconds")
            time.sleep(10)
            for event in self.long_poll.listen():
                if event.type == VkEventType.MESSAGE_NEW and event.to_me:
                    self.event = event
                    print(self.event)
                    self.__query_manager__(query)

def sender(message, vk, weather, city):
        print("WEATHER:",weather)
        if weather is not None:
            print("OK!")
            isDay = 1
            print("ISDAY", isDay)
            temp = weather['main']['temp']
            print("TEMP", temp)
            humidity = weather['main']['humidity']
            # feelslike = weather['feelslike_c']
            visibility = weather['visibility']
            min_temp = weather['main']['temp_min']
            max_temp = weather['main']['temp_max']
            print("-------------------\n")
            print(temp)
            print(humidity)
            print(visibility)
            print(min_temp)
            print(max_temp)
            shutochki = {
                'modes': {
                    'night': ['смотри осторожнее', 'ходи с фонарем', 'так темно, будто сидишь в темной комнате', '\nНочью стук в окно: \n -"Дрова нужны" \n -"Нет" \n Утром просыпаются, дров нет', '\nЧто делать, если ночью увидел летающий телевизор? Правильно, нужно стрелять в негра, который его ворует' ],
                    'day': ['так светло, будто светит лампа', 'светло как на солнце', 'светло, будто светит солнце' 'светло как днем' ]
                }
            }   
            # isDay = weather['is_day']
            lightPhraze = ""
            print("is_day->", isDay)
            if isDay == 0 :
                NightShutochki = shutochki['modes']['night']
                shutochkiLen = len(NightShutochki)
                shutochka = NightShutochki[ randint(0, shutochkiLen-1) ]
                lightPhraze="темное время суток, "+str(shutochka)
                vk.messages.send(user_id=message.user_id, message="Сейчас в городе " + str(city.title()) + " " + str(
                    math.ceil(temp)) + " по цельсию.\nВлажность воздуха  составляет около " + str(
                    math.ceil(humidity)) + "%" + "\nМинимальная температура" + str(
                    math.ceil(min_temp)) + " по цельсию" + "\nВидимость: " + str(
                    math.ceil(visibility)) + "км" + "\nСейчас " + str(lightPhraze))
                # logger("RESPONSE SENDED")

            if isDay == 1:
                dayShutochki = shutochki['modes']['day']
                shutochkiLen = len(dayShutochki)
                shutochka = dayShutochki[ randint(0, shutochkiLen-1) ]
                lightPhraze="светлое время суток, "+str(shutochka)
                # try:
                vk.messages.send(user_id=message.user_id, message="Сейчас в городе " + str(city.title()) + " " + str(math.ceil(temp)) + " по цельсию.\nВлажность воздуха  составляет около "+str(math.ceil(humidity))+"%"+"\nМинимальная температура "+ str(math.ceil(min_temp)) + " по цельсию" + "\nВидимость: "+str(math.ceil(visibility))+ "км" + "\nСейчас "+ str(lightPhraze))
                    # logger("RESPONSE SENDED")
                    # print("SENDED!\n")
                # except:
                    # logger("ERROR WHILE SENDING A RESPONSE")
                    # print("ERROR")
                    # print(1)
        else:
            print("WEaTHER", weather)
                # try:
            vk.messages.send(user_id=message.user_id, message="Такого города нету")
                    # logger('ERROR IN QUERY')
                # except:
                    # logger("ERROR WHILE SENDING A RESPONSE")
            print("ERROR")
                    # print(1)

            

def command_error(message, vk):
    vk.messages.send(user_id=message.user_id, message="Ошибка распознования команды. Нужно вводить так: погода город")
    

if __name__ == '__main__':
    queryset = [[[u"погода", u"Погода", u"Пагода", u"пагода"], sender]]
    
    def start_bot():
        try:
            bot = VKBot(token=TOKEN_VK)
        except:
            # logger("Network problems...Retry after 10 seconds")
            time.sleep(10)
            start_bot()
        try:
            print("starting...\n")
            bot.run(query=queryset)
        except:
            # logger("Starting bot problems...Retry after 10 seconds")
            time.sleep(10)
            start_bot()

start_bot()