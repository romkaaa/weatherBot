import logging

def logger(message):

    logging.basicConfig(
        filename='weatherBot.log',
        format='%(asctime)s %(message)s',
        datefmt='%m/%d/%Y %I:%M:%S %p',
        level=logging.DEBUG
        )
    logging.info(message)
